package common;

public class CommonMethods {

    private static double storedPrice;

    public void setStoredPrice(double price) {
        storedPrice = price;
    }

    public double getStoredPrice() {
        return this.storedPrice;
    }
}
