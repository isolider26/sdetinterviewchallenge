package pages;

import common.CommonMethods;
import config.DriverFactory;
import config.WaitingTimeSetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultsPage extends DriverFactory {

    @FindBy(xpath = "//span[@class='a-price-whole']")
    private WebElement priceWhole;
    @FindBy(xpath = "//span[@class='a-price-fraction']")
    private WebElement priceFraction;

    private WebElement firstResult;

    private WebDriverWait wait;
    private CommonMethods cm;

    public SearchResultsPage() {
        super();
        PageFactory.initElements(getDriver(), this);
        wait = new WebDriverWait(getDriver(), WaitingTimeSetup.getWaitForWebElement());
        cm = new CommonMethods();
    }

    public void findProductInResults(String product) {
        //WebElement e = getDriver().findElement(By.xpath("//span[contains(text(),'"+ product + "') and @class='a-size-medium a-color-base a-text-normal']"));
        firstResult = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'"+ product + "') and @class='a-size-medium a-color-base a-text-normal']")));
        cm.setStoredPrice(Double.parseDouble(priceWhole.getText()) + (Double.parseDouble(priceFraction.getText())/100));
    }

    public void clickOnFirstResult() {
        firstResult.click();
    }
}
