package pages;

import config.DriverFactory;
import config.PropertiesFile;
import config.WaitingTimeSetup;
import interfaces.EnterText;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LandingPage extends DriverFactory implements EnterText {

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchBar;
    @FindBy(xpath = "//input[@id='nav-search-submit-button']")
    private WebElement searchButton;
    @FindBy(xpath = "//*[contains(@href,'register')]")
    private WebElement registerButton;

    private WebDriverWait wait;

    public LandingPage() {
        super();
        PageFactory.initElements(getDriver(),this);
        wait = new WebDriverWait(getDriver(), WaitingTimeSetup.getWaitForWebElement());
    }

    public void openLandingPage() {
        getDriver().get(new PropertiesFile().getLandingPageUrl());
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().implicitlyWait(WaitingTimeSetup.getWaitImplicit(), TimeUnit.SECONDS);
        getDriver().manage().timeouts().pageLoadTimeout(WaitingTimeSetup.getWaitForPageLoad(),TimeUnit.SECONDS);
    }

    public void searchProduct(String product) {
        wait.until(ExpectedConditions.visibilityOf(searchBar));
        writeOnElementField(searchBar,product);
        //searchBar.sendKeys(product);
        searchButton.click();
    }

    public void openPanel() {
        Actions action = new Actions(getDriver());
        WebElement element = getDriver().findElement(By.xpath("//*[@id='nav-link-accountList']"));
        action.moveToElement(element).perform();
    }

    public void clickOnRegister() {
        wait.until(ExpectedConditions.visibilityOf(registerButton));
        registerButton.click();
    }

    @Override
    public void writeOnElementField(WebElement element, String value) {
        element.sendKeys(value);
    }
}
