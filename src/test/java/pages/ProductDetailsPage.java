package pages;

import common.CommonMethods;
import config.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;

public class ProductDetailsPage extends DriverFactory {

    @FindBy(xpath = "//span[@id='priceblock_ourprice']")
    private WebElement productPrice;
    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;
    @FindBy(xpath = "//*[@id='hlb-view-cart-announce']")
    private WebElement cartButton;

    private CommonMethods cm = new CommonMethods();

    public ProductDetailsPage() {
        super();
        PageFactory.initElements(getDriver(), this);
    }

    public void viewProductDetails() {
        try {
            String p = productPrice.getText().replaceAll("(.*\\$\\s)","");
            Double price = Double.parseDouble(p);
            //price = 12.5;
            Assert.assertEquals("Price is not the same",new BigDecimal(cm.getStoredPrice()), new BigDecimal(price));
        } catch (AssertionError e) {
            System.out.println(e.getMessage());
        }

    }

    public void clickAddToCartButton() {
        addToCartButton.click();
    }

    public void clickMoveToCart() {
        cartButton.click();
    }
}
