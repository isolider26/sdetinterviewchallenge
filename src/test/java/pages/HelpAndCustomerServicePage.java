package pages;

import config.DriverFactory;
import config.WaitingTimeSetup;
import interfaces.EnterText;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HelpAndCustomerServicePage extends DriverFactory implements EnterText {

    @FindBy(xpath = "//input[@id='helpsearch']")
    private WebElement searchBar;
    @FindBy(xpath = "//*[contains(text(),'Echo Support')]")
    private WebElement searchValueOption;
    @FindBy(xpath = "//*[contains(text(),'Amazon Echo')]")
    private WebElement searchTitle;

    private WebDriverWait wait;

    public HelpAndCustomerServicePage(){
        super();
        PageFactory.initElements(getDriver(),this);
        wait = new WebDriverWait(getDriver(), WaitingTimeSetup.getWaitForWebElement());
    }

    public void searchForSupport(String searchValue) {
        wait.until(ExpectedConditions.visibilityOf(searchBar));
        writeOnElementField(searchBar, searchValue);
        //searchBar.sendKeys(searchValue);
        searchBar.sendKeys(Keys.ENTER);

    }

    public void clickOnSearchValueOption() {
        searchValueOption.click();
        wait.until(ExpectedConditions.visibilityOf(searchTitle));

    }

    public void validateOptionsDisplayed(List<String> options) {
        for (String option : options) {
            WebElement o = getDriver().findElement(By.xpath("//*[contains(text(),'" + option +"')]"));
            Assert.assertTrue(o.isDisplayed());
        }
    }

    @Override
    public void writeOnElementField(WebElement element, String value) {
        element.sendKeys(value);
    }
}
