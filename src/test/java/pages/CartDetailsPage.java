package pages;

import common.CommonMethods;
import config.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;

public class CartDetailsPage extends DriverFactory {

    @FindBy(xpath = "//span[@id='sc-subtotal-amount-activecart']")
    private WebElement subtotal;
    @FindBy(xpath = "//input[contains(@name, 'delete')]")
    private WebElement deleteButton;
    @FindBy(xpath = "//*[@class='a-row sc-your-amazon-cart-is-empty']")
    private WebElement cartEmptyMessage;

    private CommonMethods commonMethods = new CommonMethods();

    public CartDetailsPage() {
        super();
        PageFactory.initElements(getDriver(), this);
    }

    public void validateProductPrice() {
        String p = subtotal.getText().replaceAll("(.*\\$\\s)","");
        Double price = Double.parseDouble(p);
        //price = 12.5;
        Assert.assertEquals("Price is not the same",new BigDecimal(commonMethods.getStoredPrice()), new BigDecimal(price));
    }

    public void deleteProductFromCart() {
        deleteButton.click();
        Assert.assertTrue(cartEmptyMessage.isDisplayed());
    }
}
