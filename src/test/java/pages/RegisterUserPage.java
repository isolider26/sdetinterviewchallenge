package pages;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import config.DriverFactory;
import config.PropertiesFile;
import config.WaitingTimeSetup;
import interfaces.EnterText;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class RegisterUserPage extends DriverFactory implements EnterText {

    @FindBy(xpath = "//input[@id='ap_customer_name']")
    private WebElement inputName;
    @FindBy(xpath = "//input[@id='ap_email']")
    private WebElement inputEmail;
    @FindBy(xpath = "//*[contains(@href,'condition_of_use')]")
    private WebElement conditionOfUseButton;


    private WebDriverWait wait;

    public RegisterUserPage() {
        super();
        PageFactory.initElements(getDriver(),this);
        wait = new WebDriverWait(getDriver(), WaitingTimeSetup.getWaitForWebElement());
    }

    public void fillRegistrationFormWithEmployeeData(String id) {
        try {
            String employeeName = getEmployeeDataFromApi(id);
            String employeeEmail = getEmailForEmployee(employeeName);
            wait.until(ExpectedConditions.visibilityOf(inputName));
            writeOnElementField(inputName,employeeName);
            writeOnElementField(inputEmail,employeeEmail);
            //inputName.sendKeys(employeeName);
            //inputEmail.sendKeys(employeeEmail);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clickOnConditionsOfUse() {
        conditionOfUseButton.click();
    }

    public String getEmployeeDataFromApi(String id) throws IOException, ParseException {
        //Connect to the api
        URL url = new URL(new PropertiesFile().getAPIUrl()+id);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.connect();
        if(conn.getResponseCode()!=200){
            throw new RuntimeException("HttpResponseCode: " + conn.getResponseCode());
        }else{
            //Add each line to string
            String inline = "";
            Scanner scanner = new Scanner(url.openStream());

            while (scanner.hasNext()) {
                inline += scanner.nextLine();
            }
            scanner.close();

            JSONParser parse = new JSONParser();
            JSONObject data_obj = null;
            try {
                data_obj = (JSONObject) parse.parse(inline);
            } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }
            JSONObject employeeData = (JSONObject) data_obj.get("data");
            //System.out.println(employeeData);
            return (String) employeeData.get("employee_name");
        }

    }

    private String getEmailForEmployee(String name) {
        String[] nameList = name.split(" ");
        return nameList[0] + "." + nameList[1] + "@fake.com";
    }

    @Override
    public void writeOnElementField(WebElement element, String value) {
        element.sendKeys(value);
    }
}
