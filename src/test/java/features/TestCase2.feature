@TestCase2
Feature: Test

  Background:
    Given I navigate to a webPage

  Scenario: Validate help and customer service search
    When I locate Sign In Account and List button
    Then I click on new Customer
    When I fill form with data of employee "1"
    Then I click on Conditions of use link
    When I search for "Echo" in support
    And click on search result value
    Then The following option should be available
      | Getting Started              |
      | Wi-Fi and Bluetooth          |
      | Device Software and Hardware |
      | Troubleshooting              |