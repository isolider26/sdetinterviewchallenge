@TestCase1
Feature: Test

  Background:
    Given I navigate to a webPage

  Scenario: Validate Price Consistency
    When I search for Product
    Then validate item is displayed
    When Click on the first result
    Then validate price consistency
    When I click the Add to cart button
    And I move to cart details page
    Then validate price in cart
    And Delete item from cart