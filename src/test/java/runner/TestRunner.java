package runner;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(  monochrome = true,
        features = {"src/test/java/features/"},
        plugin = { "com.cucumber.listener.ExtentCucumberFormatter:src/test/resources/reports/report.html" },
        glue = {"steps"},
        tags = {"@TestCase1, @TestCase2"})
public class TestRunner {
    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(System.getProperty("user.dir") + "\\src\\test\\java\\config\\extent-config.xml");
    }
}
