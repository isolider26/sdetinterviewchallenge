package config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

    private static WebDriver driver;
    private static String path = "src/test/resources/webdriver/chromedriver.exe";

    public DriverFactory() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", path);
            driver = new ChromeDriver();
        }
    }
    public WebDriver getDriver() {
        return this.driver;
    }

    public void destroyDriver() {
        driver.quit();
        driver = null;
    }
}
