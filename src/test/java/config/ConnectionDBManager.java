package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDBManager {

    private static String url;
    private static String driverName;
    private static String username;
    private static String password;
    private static Connection con;
    private static String urlString;

    public ConnectionDBManager() {
        PropertiesFile properties = new PropertiesFile();
        this.url = properties.getDBUrl();
        this.driverName = properties.getDBDriver();
        this.username = properties.getDBUserName();
        this.password = properties.getDBPassword();
    }

    public static Connection getConnection() {
        try {
            Class.forName(driverName);
            try {
                con = DriverManager.getConnection(url, username, password);
            } catch (SQLException ex) {
                System.out.println("Failed to create the database connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver not found");
        }

        return con;
    }
}
