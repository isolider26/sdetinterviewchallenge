package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {

    private static String url;

    Properties properties = new Properties();
    InputStream inputStream = null;

    public PropertiesFile(){
        loadBrowserProperties();
    }

    private void loadBrowserProperties(){
        try {
            inputStream = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\config\\config.properties");
            properties.load(inputStream);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getLandingPageUrl() {
        return properties.getProperty("web_url");
    }

    public String getAPIUrl() {
        return properties.getProperty("API_url");
    }

    public String getDBUrl() { return properties.getProperty("db_url");}

    public String getDBDriver() { return properties.getProperty("db_driver");}

    public String getDBUserName() { return properties.getProperty("db_username");}

    public String getDBPassword() { return properties.getProperty("db_password");}

}
