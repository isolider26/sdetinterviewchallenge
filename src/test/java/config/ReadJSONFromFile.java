package config;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class ReadJSONFromFile {

    private JSONObject searchValues;
    private static String pathFile = "src/test/resources/searchValues.json";

    public ReadJSONFromFile() {
        JSONParser parser = new JSONParser();
        try {
            FileReader reader = new FileReader(pathFile);
            searchValues = (JSONObject) parser.parse(reader);

        } catch (Exception e) {

        }
    }

    public String getValueFromJson(String key) {
        return (String) searchValues.get(key);
    }
}
