package steps;

import common.CommonMethods;
import config.DriverFactory;
import config.ReadJSONFromFile;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.*;

import java.util.List;

public class Steps {

    private DriverFactory driverManager;
    private LandingPage landingPage = new LandingPage();
    private SearchResultsPage searchResultsPage = new SearchResultsPage();
    private ProductDetailsPage productDetailsPage = new ProductDetailsPage();
    private CartDetailsPage cartDetailsPage = new CartDetailsPage();
    private RegisterUserPage registerUserPage = new RegisterUserPage();
    private HelpAndCustomerServicePage helpAndCustomerServicePage = new HelpAndCustomerServicePage();
    private CommonMethods cm = new CommonMethods();

    @Before
    public void beforeScenario() {
        driverManager = new DriverFactory();
    }

    @Given("I navigate to a webPage")
    public void navigateToWebPage(){
        landingPage.openLandingPage();
    }

    @Given("This is a test case")
    public  void testCase() throws Throwable {
        System.out.println("TEST!!!");
    }

    @Given("^I search for \"([^\"]*)\"$")
    public void searchProduct(String product) throws Throwable {
        System.out.println(product);
        landingPage.searchProduct(product);
    }

    @Given("I search for Product")
    public void searchProductFromJSON() throws Throwable {
        landingPage.searchProduct(new ReadJSONFromFile().getValueFromJson("product"));
    }

    @Given("validate item is displayed")
    public void validateItemIsDisplayed() throws Throwable{
        searchResultsPage.findProductInResults(new ReadJSONFromFile().getValueFromJson("product"));
    }

    @When("Click on the first result")
    public void clickOnFirstResult() throws Throwable {
        searchResultsPage.clickOnFirstResult();
    }

    @Then("validate price consistency")
    public void viewProductDetails() throws Throwable {
        productDetailsPage.viewProductDetails();
    }

    @Then("I click the Add to cart button")
    public void ClickAddToCartButton() throws Throwable{
        productDetailsPage.clickAddToCartButton();
    }

    @When("I move to cart details page")
    public void moveToCartDetailsPage() throws Throwable {
        productDetailsPage.clickMoveToCart();
    }

    @And("validate price in cart")
    public void validateCartPrice() throws Throwable {
        cartDetailsPage.validateProductPrice();
    }

    @Then("Delete item from cart")
    public void deleteItemFromCart() throws Throwable {
        cartDetailsPage.deleteProductFromCart();
    }

    @When("I locate Sign In Account and List button")
    public void clickOnSignIn() throws Throwable {
        landingPage.openPanel();
    }

    @Then("I click on new Customer")
    public void clickOnNewCustomer() throws Throwable{
        landingPage.clickOnRegister();
    }

    @When("^I fill form with data of employee \"([^\"]*)\"$")
    public void fillFormWithDataOfEmployee(String id) throws Throwable {
        registerUserPage.fillRegistrationFormWithEmployeeData(id);
    }

    @Then("I click on Conditions of use link")
    public void clickOnConditionsOfUse() throws Throwable {
        registerUserPage.clickOnConditionsOfUse();
    }

    @When("^I search for \"([^\"]*)\" in support$")
    public void searchValueInSupport(String value) throws Throwable {
        helpAndCustomerServicePage.searchForSupport(value);
    }

    @When("I search for support")
    public void searchValueInSupport() throws Throwable {
        helpAndCustomerServicePage.searchForSupport(new ReadJSONFromFile().getValueFromJson("support"));
    }

    @And("click on search result value")
    public void clickOnSearchResultValue() throws Throwable {
        helpAndCustomerServicePage.clickOnSearchValueOption();
    }

    @Then("The following option should be available")
    public void validateOptionsAvailable(DataTable table) throws Throwable {
        List<String> values = table.asList(String.class);
        helpAndCustomerServicePage.validateOptionsDisplayed(values);
    }


    @After
    public void afterScenario() {
        driverManager.destroyDriver();
    }
}
