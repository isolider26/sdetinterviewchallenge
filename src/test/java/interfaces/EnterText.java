package interfaces;

import org.openqa.selenium.WebElement;

public interface EnterText {

    void writeOnElementField(WebElement element, String value);
}
