$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("TestCase2.feature");
formatter.feature({
  "line": 2,
  "name": "Test",
  "description": "",
  "id": "test",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@TestCase2"
    }
  ]
});
formatter.before({
  "duration": 3926491000,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I navigate to a webPage",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.navigateToWebPage()"
});
formatter.result({
  "duration": 24540933300,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Test",
  "description": "",
  "id": "test;test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I locate Sign In Account and List button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I click on new Customer",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I fill form with data of employee \"1\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I click on Coditions of use link",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I search for support",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "click on \"Echo Support\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "elemets should be visible",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.clickOnSignIn()"
});
formatter.result({
  "duration": 336606400,
  "status": "passed"
});
formatter.match({
  "location": "Steps.clickOnNewCustomer()"
});
formatter.result({
  "duration": 1069255100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 35
    }
  ],
  "location": "Steps.fillFormWithDataOfEmployee(String)"
});
formatter.result({
  "duration": 3092107600,
  "status": "passed"
});
formatter.match({
  "location": "Steps.clickOnConditionsOfUse()"
});
formatter.result({
  "duration": 1359675400,
  "status": "passed"
});
formatter.match({
  "location": "Steps.searchValueFromJSON()"
});
formatter.result({
  "duration": 1351176000,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "duration": 70100,
  "status": "passed"
});
});